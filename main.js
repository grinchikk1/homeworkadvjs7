class WhackAMole {
  constructor() {
    this._props = {
      field: {
        // Ігрове поле
        rows: 10,
        cols: 10,
      },
      level: [
        // Рівень складності
        {
          name: "light",
          displayName: "Легкий",
          timeout: 1500,
        },
        {
          name: "medium",
          displayName: "Середній",
          timeout: 1000,
        },
        {
          name: "hard",
          displayName: "Важкий",
          timeout: 500,
        },
      ],
      color: {
        // Колір кнопки
        current: "blue",
        user: "green",
        computer: "red",
      },
    };
    this._data = {
      // Дані гри
      level: 0,
    };
    this._html = {};
    this._init();
    this._createUI();
    this._listenNewGameButton();
  }

  _init() {
    // Визначення кольору кнопки
    if (this._data.timer) clearTimeout(this._data.timer);
    if (document.querySelector(".endGameMsg"))
      document.querySelector(".endGameMsg").remove();
    this._setParamLevel();
    this._resetDataScore();
    this._generateNewDataField();
  }

  appendTo(parent) {
    parent.append(this._html.container);
  }

  // створення нового ігрового поля
  _generateNewDataField() {
    this._data.uncheckedCells = [];
    for (let i = 0; i < this._props.field.rows; i++) {
      for (let j = 0; j < this._props.field.cols; j++) {
        this._data.uncheckedCells.push({ row: i, col: j });
      }
    }
  }

  // прослуховувач кнопки "Нова гра"
  _listenNewGameButton() {
    this._startNewGameBound = this.startNewGame.bind(this);
    this._html.btnNewGame.addEventListener("click", this._startNewGameBound);
  }

  // встановлення прослуховувача для ігрового поля
  _listenFieldTable() {
    this._userTableClickBound = this._userTableClick.bind(this);
    this._html.table.addEventListener("click", this._userTableClickBound);
  }

  // натиснута кнопка "Нова гра"
  startNewGame() {
    this._resetHtmlTable();
    this._init();
    this._listenFieldTable();
    this._computerPlay();
  }

  // гравець зробив свій хід (клікнув по полю)
  _userTableClick(e) {
    if (e.target === this._html.currentCell) {
      clearTimeout(this._data.timer);
      this._incDataScore("human");
      this._updateHtmlScore();
      this._markCell(this._html.currentCell, this._props.color.user);

      if (this._isGameOver()) {
        this._gameOver();
        return;
      }
      this._computerPlay();
    }
  }

  // змінити поточний рівень складності
  _setParamLevel() {
    if (document.querySelector(".paramInput:checked")) {
      this._data.level = document.querySelector(
        ".paramInput:checked"
      ).dataset.levelId;
    } else {
      this._data.level = 0;
    }
  }

  // Повертає таймаут поточного рівня складності
  _getlevelTimeout() {
    return this._props.level[this._data.level].timeout;
  }

  // Повертає координату випадкової неперевіреної комірки ігрового поля
  _getDataRandomUncheckedCellCoord() {
    const rndIndex = Math.floor(
      Math.random() * this._data.uncheckedCells.length
    );
    const rndCell = this._data.uncheckedCells[rndIndex];
    return { cell: rndCell, cellId: rndIndex };
  }

  // Повертає HTML-елемент ігрової комірки з заданими координатами
  _getHtmlCell(coord) {
    return this._html.table.children[coord.row].children[coord.col];
  }

  // Позначити комірку кольором
  _markCell(htmlCell, color) {
    htmlCell.style.backgroundColor = color;
  }

  // Перевірка на умову кінця гри
  _isGameOver() {
    const halfCellNumber =
      (this._props.field.rows * this._props.field.cols) / 4;
    return (
      this._data.score.human >= halfCellNumber ||
      this._data.score.computer >= halfCellNumber
    );
  }

  // Повертає переможця
  _whoWin() {
    return this._data.score.human > this._data.score.computer
      ? "Гравець"
      : "Компʼютер";
  }

  // Кінець гри
  _gameOver() {
    clearTimeout(this._data.timer);
    this._html.table.removeEventListener("click", this._userTableClickBound);
    const htmlWin = document.createElement("p");
    htmlWin.classList.add("endGameMsg");
    htmlWin.textContent = `${this._whoWin()} Переміг!!!`;
    htmlWin.style.width = this._html.table.offsetWidth + "px";
    this._html.container.append(htmlWin);
    htmlWin.style.top =
      this._html.table.offsetTop +
      this._html.table.offsetHeight / 2 -
      htmlWin.offsetHeight / 2 +
      "px";
  }

  // Хід комп'ютера - вибір випадкової комірки
  _computerPlay() {
    this._data.currentCell = this._getDataRandomUncheckedCellCoord();
    this._html.currentCell = this._getHtmlCell(this._data.currentCell.cell);
    this._markCell(this._html.currentCell, this._props.color.current);
    this._data.uncheckedCells.splice(this._data.currentCell.cellId, 1);
    this._data.timer = setTimeout(
      this._timeOver.bind(this),
      this._getlevelTimeout()
    );
  }

  // Час гравця вийшов
  _timeOver() {
    this._incDataScore("computer");
    this._updateHtmlScore();
    this._markCell(this._html.currentCell, this._props.color.computer);
    if (this._isGameOver()) {
      this._gameOver();
      return;
    }
    this._computerPlay();
  }

  // Обнулити дані рахунку гри
  _resetDataScore() {
    this._data.score = {
      human: 0,
      computer: 0,
    };
  }

  // Збільшити дані рахунку гри
  _incDataScore(player) {
    this._data.score[player]++;
  }

  // Оновити інтерфейс рахунку гри
  _updateHtmlScore() {
    this._html.score.innerText = `Рахунок: ${this._data.score.human} - Гравець / ${this._data.score.computer} - Компʼютер`;
  }

  // Створення ігрового інтерфейсу
  _createUI() {
    // Контейнер гри
    this._html.container = document.createElement("div");
    this._html.container.classList.add("whack-Mole-Game");
    // Кнопка нової гри
    this._html.btnNewGame = document.createElement("button");
    this._html.btnNewGame.innerText = "Почати Нову Гру";
    this._html.btnNewGame.classList.add("whack-Mole-Game__btn");
    this._html.container.append(this._html.btnNewGame);
    // Рахунок гри
    this._html.score = document.createElement("p");
    this._html.score.classList.add("whack-Mole-Game__score");
    this._html.score.innerText = `Рахунок: ${this._data.score.human} - Гравець / ${this._data.score.computer} - Компʼютер`;
    this._html.container.append(this._html.score);
    // Ігрове поле
    this._html.table = document.createElement("table");
    this._html.table.classList.add("whack-Mole-Game__table");
    this._html.container.append(this._html.table);

    for (let i = 0; i < this._props.field.rows; i++) {
      let tr = document.createElement("tr");
      this._html.table.append(tr);
      for (let j = 0; j < this._props.field.cols; j++) {
        let td = document.createElement("td");
        td.classList.add("whack-Mole-Game__cell");
        tr.append(td);
      }
    }

    // Параметри рівня складності
    this._html.param = document.createElement("form");
    this._html.param.classList.add("whack-Mole-Game__params");
    this._html.container.append(this._html.param);
    const elParamSet = document.createElement("fieldset");
    this._html.param.append(elParamSet);
    elParamSet.append(document.createElement("legend"));
    elParamSet.firstElementChild.textContent = "Рівень складності: ";
    this._props.level.forEach((levelItem, id) => {
      const elParamRowsInput = document.createElement("input");
      elParamRowsInput.classList.add("paramInput");
      elParamRowsInput.id = `input-${levelItem.name}`;
      elParamRowsInput.dataset.levelId = id;
      elParamRowsInput.value = levelItem.timeout;
      this._data.level === id ? (elParamRowsInput.checked = "true") : false;
      elParamRowsInput.name = "level";
      elParamRowsInput.type = "radio";
      elParamSet.append(elParamRowsInput);
      let elParamLabel = document.createElement("label");
      elParamLabel.classList.add("paramLabel");
      elParamLabel.setAttribute("for", `input-${levelItem.name}`);
      elParamLabel.textContent = levelItem.displayName;
      elParamSet.append(elParamLabel);
    });
  }

  // скидання стану таблиці гри
  _resetHtmlTable() {
    const cells = document.querySelectorAll(".whack-Mole-Game__cell");
    [...cells].forEach((cell) => (cell.style.backgroundColor = ""));
  }
}

const newGame = new WhackAMole();
newGame.appendTo(document.body);
